// alert("Hello Wordl!");

// Arithmetic Operators
let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;
console.log("Result of addition operators: "+ sum);

// Subtraction Operator
let difference = x - y;
console.log("Result of the subtraction operator: "+ difference);

// Multiplication Operator
let product = x * y;
console.log("Result of the multiplication operator: "+ product);

// Division Operator
let quotient = x / y;
console.log("Result of the division operator: "+ quotient);

// Modulo Operator
let remainder = y % x;
console.log("Result of the modulo operator: "+remainder);

// Assignment Operator

// Basic Assignment Operator (=)
/*
	The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
*/
let assignmentNumber = 8;

// Addition Assignment Operator
/*
	uses the current value of the variable and ADDS a number to itself. Afterwards it reassigns it as a new value.

	assignmentNumber = assignmentNumber + 2;
*/
assignmentNumber += 2;
console.log("Result of the addition assignment operator: "+ assignmentNumber); //10

// Subraction || Multiplication || Division ( -=, *=, /=)

// Subtraction Assignment Operator
assignmentNumber -= 2;
console.log("Result of the subtraction assignment operator: "+ assignmentNumber); //8

// Multiplication Assignment Operator
assignmentNumber *= 2;
console.log("Result of the multiplication assignment operator: "+ assignmentNumber); //16

// Division Assignment Operator
assignmentNumber /= 2;
console.log("Result of the division assignment operator: "+ assignmentNumber); //8

// Multiple Operators and Parenthesis
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS rule (Parenthesis, Exponents, Multiplication, Division, Addition, and Subtraction)
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: "+ mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: "+ pemdas);

// Increment and Decrement
/*
	Operators that add and subtract values by 1 and reassings the value of the variable where the increment/decrement was applied to
*/

let z = 1;

// Pre-increment
/*
	The value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment";
*/
let preIncrement = ++z;
console.log("Result of the pre-increment: "+ preIncrement); //2
console.log("Result of the pre-increment: "+ z); //2

// Post increment
/*
	The value of "z" is returned and stored in the variable "postIncrement" then the value of "z" is increased by one
*/
let postIncrement = z++;
console.log("Result of the post-increment: "+ postIncrement); //1
console.log("Result of the post-increment: "+ z); //2

/*
	Pre-increment - adds 1 first before reading value
	Post-increment - reads value first before we adding 1

	Pre-decrement - subtracts 1 first before reading value
	Post-decrement - reads value first before we subtracting 1
*/

let a = 2;

// Pre-Decrement
/*
	The value of "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecrement"
*/
let preDecrement = --a;
console.log("Result of the pre-decrement: "+ preDecrement);
console.log("Result of the pre-decrement: "+ a);

// Post-Decrement
/*
	The value "a" is returned and stored in the variable "postDecrement" then the value of "a" is decreased by 1
*/
let postDecrement = a--;
console.log("Result of the post-decrement: "+ postDecrement);
console.log("Result of the post-decrement: "+ postDecrement);

// Type Coercion
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another
*/

let numA = '10';
let numB = 12;

/*
	Adding/concatenating a string and a number will result as a string	
*/

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion); //String

let coercion1 = numA - numB;
console.log(coercion1); // -2
console.log(typeof coercion1); //Number

// Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Addition of Number and Boolean
/*
	The result is a number
	The boolean "true" is associated with the value of 1
	The boolean "false" is associated with the value of 0
*/

let numE = true + 1;
console.log(numE); //2
console.log(typeof numE); //number

let numF = false + 1;
console.log(numF); //1

// Comparison Operator
let juan = 'juan';

// Equality Operator (==)
/*
	Checks whether the operands are equal/have the same content
	Attempts to CONVERT and COMPARE operands of different data types
	Returns a boolean value (true/false)
*/

console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(1 == true); //true
console.log( 'juan' == 'juan'); //true
console.log('true' == true); //false
console.log(juan == 'juan'); //true

// Inequality Operator (!=)
/*
	Checks whether the operands are not equal/have different content
	Attempts to CONVERT and COMPARE operands of different data types
*/
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(1 != true); //false
console.log('juan' != 'juan'); //false
console.log('juan' != juan); //false

// Strict Equality Operator(===)
/*
	Checks whether the operands are equal/have the same content
	Also COMPARES the data types of 2 values
*/
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false
console.log(1 === true); //false
console.log('juan' === 'juan'); //true
console.log(juan === 'juan'); //true

// Strict Inequality Operator (!==)
/*
	Checks whether the operands are not equal/have the same content
	Also COMPARES the data types of 2 values
*/

console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(1 !== true); //true
console.log('juan' !== 'juan'); //false
console.log(juan !== 'juan'); //false

// Relational Operator
// returns a boolean value
let j = 50;
let k = 65;

// Greater than operator (>)
let isGreaterThan = j > k ;
console.log(isGreaterThan); //false

// Less than operator (<)
let isLessThan = j < k;
console.log(isLessThan); //true

// Greater than or Equal operator (>=)
let isGToEqual = j >= k;
console.log(isGToEqual); //false

// Less than or Equal operator (<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual); //true

let numStr = "30";
// forced coercion to change the string to a number
console.log(j > numStr); //true

let str = "thirty";
// 50 is greater then NaN
console.log(j > str); //false

// Logical Operators
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& Double Ampersand)
/*
	Returns true if all operands are true
	true && true = true
	true && false = false
*/
let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet); //false

// Logical OR Operator (|| Double Pipe)
/*
	Returns true if one of the operands are true
	true || true = true
	true || false = true
	false || false = false
*/
let someRequirementMet = isLegalAge || isRegistered;
console.log(someRequirementMet); //true

// Logical NOT Operator (!)
let someRequirementNotMet = !isRegistered;
// Returns the opposite value
console.log(someRequirementNotMet); //true